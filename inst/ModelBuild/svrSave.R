# contains the commands to save various combinations of the analysis

######################################
# save the entire analysis
output$bttnSave <- downloadHandler(
    filename = "Workspace.rds",
    ## function(fname = input$downloadFileName){
    ##     if(fname  == ""){
    ##         fname  <-  "Workspace.rds"
    ##     }else{
    ##          if(tolower(substr(fname,nchar(fname)-4,nchar(fname)))!=".rds"){
    ##              fname  <-  paste(fname,"rds",sep=".")
    ##          }
    ##      }
    ##     return(fname)
    ## },
    content = function(file)   {
        saveRDS(isolate({reactiveValuesToList(analysisRecord)}),file=file)
  }
)

## save a model 
output$bttnSaveMdl <- downloadHandler(
    filename = function(fname=input$selSaveMdl){
        fname <- paste0(fname,'_mdl.rds')
    },
    
    ## function(fname = input$downloadFileName){
    ##     if(fname  == ""){
    ##         fname  <-  "Workspace.rds"
    ##     }else{
    ##          if(tolower(substr(fname,nchar(fname)-4,nchar(fname)))!=".rds"){
    ##              fname  <-  paste(fname,"rds",sep=".")
    ##          }
    ##      }
    ##     return(fname)
    ## },
    content = function(file)   {
        tmp <- analysisRecord$mdl[[input$selSaveMdl]]$param
        saveRDS(tmp,file=file)
  }
)

## save a model 
output$bttnSaveFrcst <- downloadHandler(
    filename = function(fname=input$selSaveMdl){
        fname <- paste0(fname,'_frcst.rds')
    },
    
    ## function(fname = input$downloadFileName){
    ##     if(fname  == ""){
    ##         fname  <-  "Workspace.rds"
    ##     }else{
    ##          if(tolower(substr(fname,nchar(fname)-4,nchar(fname)))!=".rds"){
    ##              fname  <-  paste(fname,"rds",sep=".")
    ##          }
    ##      }
    ##     return(fname)
    ## },
    content = function(file)   {
        tmp <- analysisRecord$mdl[[input$selSaveMdl]]
        tmp <- list(cal = tmp[['cal']],
                    val = tmp[['val']])
        saveRDS(tmp,file=file)
  }
)
