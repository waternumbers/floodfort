## ###################
## Update date selectors when data is loaded
## ###################
observe({
    tmp <- analysisRecord$baseData
    nms <- names(tmp)
    
    isolate({
        
        if(is.null(nms)){ return() }
        
        ## time ranges for data
        dateList <- c("dateCalibRng","dateValidRng") ##,"dateCalibMax",
        for(ii in dateList){
            sf <- switch(ii,
                         dateCalibRng = c(1,floor(nrow(tmp)/2)),
                         dateValidRng = c(floor(nrow(tmp)/2)+1,nrow(tmp)))
            updateDateRangeInput(session, ii,
                                 label = NULL,
                                 start = substr(index(tmp[sf[1]]),1,10),
                                 end = substr(index(tmp[sf[2]]),1,10),
                                 min = substr(index(tmp[1]),1,10),
                                 max = substr(index(tmp[nrow(tmp)]),1,10)
                                 )
        }
    })
})
