## ###################
## Data Import Tab
## ###################
fluidRow(
    column(
        12,
        fluidRow(
            column(
                11,p("Please load the data used to generate the flood forecasting model")),
            column(
                1,actionLink("helpImport",NULL,icon("question-circle")))),
        fluidRow(
            column(
                4,
                wellPanel(
                    fileInput("fileData", 'Choose Data File',
                              accept=c('text/csv', 
                                  'text/comma-separated-values,text/plain', '.csv')
                              ),
                    radioButtons('radSepData', 'Separator',
                                 c(Comma=',',
                                   Semicolon=';',
                                   Tab='\t'),
                                 ','
                                 ),
                    radioButtons('radQuoteData', 'Quote',
                                 c(None='',
                                   'Double Quote'='"',
                                   'Single Quote'="'"),
                                 '"'
                                 )                
                    )
                ),
            column(
                8,
                wellPanel(
                    fluidRow(
                        column(
                            8,
                            selectInput("selData",
                                        "Select variables:",
                                        "",
                                        selected = "",
                                        multiple=TRUE)),
                        column(
                            4,
                            checkboxInput('ckScaleData', 'Scale Plot?', FALSE)))),
                fluidRow(
                    column(
                        12,
                        dygraphOutput("plotData")))
                )
            )
        )
    )
